import { ref } from 'vue';
import { defineComponent } from 'vue';
import { loadSecurityMicro } from '@jecloud/utils';
/**
 * 资源表添加密级字段
 */
export default defineComponent({
  setup(props, { expose }) {
    let rendered = ref(false);
    let security = null;
    loadSecurityMicro().then((plugin) => {
      security = plugin;
      rendered.value = !!plugin;
    });

    const $menu = {
      key: 'security',
      addField(options) {
        return security?.addTableSecurityField(options);
      },
    };
    expose($menu);

    return () => (rendered.value ? security.renderTableMenu({ key: $menu.key }) : null);
  },
});
