import { ref } from 'vue';
import TableImport from '@/components/import/index.vue';
import { Modal } from '@jecloud/ui';
import { importTables } from '@/api/index';

export function showImportWin({ modalData, productData, callback }) {
  const $plugin = ref();
  const pluginSlot = () => {
    return (
      <TableImport ref={$plugin} modalData={modalData} productData={productData}></TableImport>
    );
  };
  // 确定按钮
  const onOkButtonClick = ({ $modal, button }) => {
    const $grid = $plugin.value?.$grid;
    const records = $grid?.getCheckboxRecords();
    if (records.length > 0) {
      const tables = [];
      records.forEach((rec) => {
        tables.push(rec.tableCode);
      });
      // 导入接口
      importTables(
        { tables: tables.join(','), SY_PRODUCT_ID: productData.productId, SY_PARENT: modalData.id },
        { pd: productData.productCode },
      )
        .then(() => {
          callback();
          $modal.close();
          button.loading = false;
        })
        .catch((e) => {
          button.loading = false;
          Modal.alert(e.message, 'error');
        });
    } else {
      Modal.alert('请选择数据！', 'warning');
      button.loading = false;
    }
  };
  Modal.window({
    title: '导入',
    width: '60%',
    height: '70%',
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;',
    content: () => pluginSlot(),
    okButton: {
      handler: ({ $modal, button }) => {
        button.loading = true;
        onOkButtonClick({ $modal, button });
        return false;
      },
    },
    cancelButton: true,
  });
}
